#include <iostream>
#include <vector>

using namespace std;

// пример series

int main()
{
    int n, k, buf, res = -1;
    vector<int> mas;

    cin >> n;
    cin >> k;

    for (int i = 0; i < k; i++) {
        // ввод очередного набора
        for (int j = 0; j < n; j++) {
            cin >> buf;
            mas.push_back(buf);
        }

        // обработка текущего набора (поиск элемента == 2)
        for (int j = 0; j < n; j++) {
            if (mas[j] == 2) {
                res = j; // позицию элемента == 2 пишем сюда. Если мы сюда ни разу не попадем, res останется равным -1
                break;
            }
        }
        cout << res;
        mas.clear();
    }
    return 0;
}
