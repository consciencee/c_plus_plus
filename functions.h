#ifndef FUNCTIONS_H
#define FUNCTIONS_H


float power(float e, float pow);
void power3A(float a, float *res);

void trianglePS(float a, float *P, float *S);
void swap(float *a, float *b);

void shiftl(float *a, float *b, float *c);

#endif // FUNCTIONS_H
