#include <iostream>
#include <vector>
#include <string>

using namespace std;

typedef struct MyString{
    vector<char> data; // или любой другой контейнер
    MyString(){ data.push_back('\0'); } // инициализируем пустую строку! Она будет состоять из одного терминального нуля
    MyString(string newData);
    // MyString(vector<char> newData); // прототипы "перегруженных" конструкторов
    // MyString(const char* newData);
    // MyString(MyString newData); // копирующий конструктор

    int size();
    char get(int i);
    MyString concat(MyString str1, MyString str2);

}MyString;

MyString::MyString(string newData){
    // TODO
}

int MyString::size(){
    int i = 0;
    while (data[i] != '\0') {
        i++;
    }

    return i;
}

char MyString::get(int i){
    return data[i];
}

MyString MyString::concat(MyString str1, MyString str2){
    // TODO
}

// рекурсивный переворот строки - функция. Надо написать такую же для MyString
string splitReverse(string str);

int main()
{
    cout << "Hello World!" << endl;
    return 0;
}

// если что - мы пока не учитываем нуль-байты в конце.
// Сейчас наша функция сработает так, что нуль-байт в итоге окажется в самом начале
// По логике это верно - строку-то мы перевернули))
string splitReverse(string str){
    string str1, str2;
    if(str.size() < 2)
        return str;
    // split - процедура, которая разбивает исходную строку str на 2 части и записывает их в str1 и str2
    // напиши ее прототип и реализацию
    // разбиение можешь делать как тебе удобно - можешь хоть по одному символу с начала отщипывать, а остальное класть в str2
    // кстати, в этом случае не надо будет делать + splitReverse(str1) в return, будет просто + str1
    split(str, &str1, &str2);
    return splitReverse(str2) + splitReverse(str1);
}

