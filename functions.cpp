#include "functions.h"
#include <math.h>

float power(float e, float pow){
    float res = 1;
    for (int i = 0; i < pow; i++)
        res = res * e;
 return res;
}

void power3A(float a, float *res){
    *res = power(a, 3);
}

void trianglePS(float a, float *P, float *S){
    *P = 3 * a;
    *S = a * a * sqrt(3)/4;
}

void swap(float *a, float *b){
    float tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}

void shiftl(float *a, float *b, float *c){
    float tmp;
    tmp = *a;
    *a = *b;
    *b = *c;
    *c = tmp;
}
