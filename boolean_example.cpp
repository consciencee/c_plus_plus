#include <iostream>

using namespace std;

// Считаем, что клетка 1,1 - черная

int main()
{
    int x, y;
    cin >> x >> y;

    if(!(((x >= 1) && (x <= 8)) && ((y >= 1) && (y <= 8)))){ // Проверка на то, что пользователь ввел правильные координаты
        return 1;
    }

    if ((x +y)%2 == 0)
        cout << "Black" << endl;
    else
        cout << "White" << endl;

    return 0;
}

